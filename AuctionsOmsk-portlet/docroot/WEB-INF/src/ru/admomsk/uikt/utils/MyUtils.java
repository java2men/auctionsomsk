package ru.admomsk.uikt.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.time.DateFormatUtils;

public class MyUtils {
	
	@SuppressWarnings("finally")
	static public String convertSDF (String inputStr, SimpleDateFormat inputSDF, SimpleDateFormat outputSDF) {
		String outputStr = null;
		
		try {
			//преобразуем в нужный формат даты 
			outputStr = outputSDF.format(inputSDF.parse(inputStr));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			outputStr = null;
		} finally {
			return outputStr;
		}
	} 
}
