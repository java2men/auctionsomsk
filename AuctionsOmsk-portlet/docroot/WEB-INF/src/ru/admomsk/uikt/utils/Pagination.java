package ru.admomsk.uikt.utils;

public class Pagination {
	
	protected String url;
	protected String value;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
