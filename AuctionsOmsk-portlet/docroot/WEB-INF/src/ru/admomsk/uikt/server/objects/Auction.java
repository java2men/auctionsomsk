package ru.admomsk.uikt.server.objects;

import java.util.ArrayList;

public class Auction {
	
	protected String id;
	protected String day;
	protected String month;
	protected String year;
	protected String date;
	protected String lots;
	protected ArrayList<OneDoc> onedocs;
	protected ArrayList<Objects> objects;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLots() {
		return lots;
	}
	public void setLots(String lots) {
		this.lots = lots;
	}
	public ArrayList<OneDoc> getOnedocs() {
		return onedocs;
	}
	public void setOnedocs(ArrayList<OneDoc> onedocs) {
		this.onedocs = onedocs;
	}
	public ArrayList<Objects> getObjects() {
		return objects;
	}
	public void setObjects(ArrayList<Objects> objects) {
		this.objects = objects;
	}
	/*public String getCountLots() {
		int count = 0;
		if (objects != null) {
			for (Objects object : objects) {
				count += object.getLots().size();
			}
		}
		return String.valueOf(count);
	}*/
}
