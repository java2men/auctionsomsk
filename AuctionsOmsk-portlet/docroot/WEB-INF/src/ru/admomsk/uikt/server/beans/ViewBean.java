package ru.admomsk.uikt.server.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;

import ru.admomsk.uikt.server.AuctionsOmsk;
import ru.admomsk.uikt.server.objects.Auction;
import ru.admomsk.uikt.server.objects.Objects;
import ru.admomsk.uikt.server.objects.Paginator;
import ru.admomsk.uikt.utils.Pagination;

public class ViewBean implements Serializable{
	
	private static final long serialVersionUID = 4207598399090191556L;
	public static String instanceId;
	protected static String filingValue = "filing_value";
	protected static String processingValue = "processing_value";
	protected static String resultValue = "result_value";
	protected static String archiveValue = "archive_value";
	//protected PortletRequest renderRequest;
	protected PortletResponse renderResponse;
	
	protected PortletURL filingActionURL;
	protected PortletURL processingActionURL;
	protected PortletURL resultActionURL;
	protected PortletURL filingURL;
	protected PortletURL processingURL;
	protected PortletURL resultURL;
	protected PortletURL archiveURL;
	protected String currentURL;
	protected ArrayList<Auction> auctionsFiling;
	protected ArrayList<Auction> auctionsProcessing;
	protected ArrayList<Auction> auctionsResult;
	protected ArrayList<Auction> auctionsArchive;
	protected Paginator paginator;
	protected ArrayList<Auction> pageAuctionsArchive;
	protected String filingURLForTab;
	protected String processingURLForTab;
	protected String resultURLForTab;
	
	protected String searchFromAuction = "";
	protected String searchToAuction = "";
	
	/*protected String currentPageArchive;
	protected String urlNextPageArchive;
	protected String urlPreviousPageArchive;
	protected String urlStartPageArchive;
	protected String urlEndPageArchive;
	protected ArrayList<Pagination> paginationPageArchive;
	//количество страниц в пагинаторе
	protected String stepPaginationPageArchive;
	protected boolean switchPaginationArchive;*/
	protected static final String[] strMonth = {"января", "февраля", "марта", "апреля", 
								   "мая", "июня", "июля", "августа", 
								   "сентября", "октября", "ноября", "декабря"};
	protected static String contextPath;
	
	public static String getInstanceId() {
		return instanceId;
	}
	public static String getFilingValue() {
		return filingValue;
	}
	public static String getProcessingValue() {
		return processingValue;
	}
	public static String getResultValue() {
		return resultValue;
	}
	public static String getArchiveValue() {
		return archiveValue;
	}
	
	/*public void setRenderRequest(PortletRequest renderRequest) {
		this.renderRequest = renderRequest;
	}
	public PortletRequest getRenderRequest() {
		return renderRequest;
	}*/
	
	public PortletURL getFilingActionURL() {
		return filingActionURL;
	}
	public void setFilingActionURL(PortletURL filingActionURL) {
		this.filingActionURL = filingActionURL;
	}
	public PortletURL getProcessingActionURL() {
		return processingActionURL;
	}
	public void setProcessingActionURL(PortletURL processingActionURL) {
		this.processingActionURL = processingActionURL;
	}
	public PortletURL getResultActionURL() {
		return resultActionURL;
	}
	public void setResultActionURL(PortletURL resultActionURL) {
		this.resultActionURL = resultActionURL;
	}
	
	public PortletURL getFilingURL() {
		return filingURL;
	}
	public void setFilingURL(PortletURL filingURL) {
		this.filingURL = filingURL;
	}
	public PortletURL getFilingURL(String numberPage) {
		/*Map parameterMap = filingURL.getParameterMap();
		String[] saveValue = (String[]) parameterMap.get(AuctionsOmsk.getCurrentPage());*/
		/*String newUrl = filingURL.toString().replace(AuctionsOmsk.getCurrentPage() + "=" + saveValue[0], AuctionsOmsk.getCurrentPage() + "=" + numberPage);*/
		/*filingURL.setParameter(AuctionsOmsk.getCurrentPage(), numberPage);
		String newUrl = filingURL.toString();
		return newUrl;*/
		
		/*String newUrl = filingURL.toString().replace(AuctionsOmsk.getCurrentPage() + "=" + renderRequest.getParameter(AuctionsOmsk.getCurrentPage()), AuctionsOmsk.getCurrentPage() + "=" + numberPage);
		return newUrl;*/
		filingURL.setParameter(AuctionsOmsk.getCurrentPage(), numberPage);
		return filingURL;
	}
	/*public String getFilingURL(String numberPage, String date_from, String date_to) {
		Map parameterMap = filingURL.getParameterMap();
		String[] saveValue = (String[]) parameterMap.get(AuctionsOmsk.getCurrentPage());
		String newUrl = filingURL.toString().replace(AuctionsOmsk.getCurrentPage() + "=" + saveValue[0], AuctionsOmsk.getCurrentPage() + "=" + numberPage + "");
		return newUrl;
	}*/
	public void setFilingURLForTab(String filingURLForTab) {
		this.filingURLForTab = new String(filingURLForTab);
	}
	public String getFilingURLForTab() {
		/*Map parameterMap = filingURL.getParameterMap();
		String[] saveValue = (String[]) parameterMap.get(AuctionsOmsk.getSearchAuctions());
		String newUrl = filingURL.toString().replace(AuctionsOmsk.getSearchAuctions() + "=" + saveValue[0], AuctionsOmsk.getSearchAuctions() + "=true");*/
		/*filingURL.setParameter(AuctionsOmsk.getCurrentPage(), "1");
		filingURL.setParameter(AuctionsOmsk.getSearchAuctions(), "false");
		filingURL.removePublicRenderParameter(AuctionsOmsk.getFromSearchAuction()) setParameter(FROM_SEARCH_AUCTION, renderRequest.getParameter(FROM_SEARCH_AUCTION));
		filingURL.getFilingURL().setParameter(TO_SEARCH_AUCTION, renderRequest.getParameter(TO_SEARCH_AUCTION));*/
		
		return filingURLForTab;
	}
	/*public String getFilingURLSearchAuctions() {
		String newUrl = filingURL.toString().replace(AuctionsOmsk.getSearchAuctions() + "=" + renderRequest.getParameter(AuctionsOmsk.getSearchAuctions()), AuctionsOmsk.getSearchAuctions() + "=true");
		newUrl = newUrl.replace(AuctionsOmsk.getCurrentPage() + "=" + renderRequest.getParameter(AuctionsOmsk.getCurrentPage()), AuctionsOmsk.getCurrentPage() + "=1");
		if (!renderRequest.getParameter(AuctionsOmsk.getFromSearchAuction()).equals("null")) {
			newUrl = newUrl.replace(AuctionsOmsk.getFromSearchAuction() + "=" + renderRequest.getParameter(AuctionsOmsk.getFromSearchAuction()), AuctionsOmsk.getFromSearchAuction() + "=null");
		}
		if (!renderRequest.getParameter(AuctionsOmsk.getToSearchAuction()).equals("null")) {
			newUrl = newUrl.replace(AuctionsOmsk.getToSearchAuction() + "=" + renderRequest.getParameter(AuctionsOmsk.getToSearchAuction()), AuctionsOmsk.getToSearchAuction() + "=null");
		}
		
		return newUrl; 
	}*/
	public PortletURL getProcessingURL() {
		return processingURL;
	}
	public void setProcessingURL(PortletURL processingURL) {
		this.processingURL = processingURL;
	}
	public PortletURL getProcessingURL(String numberPage) {
		processingURL.setParameter(AuctionsOmsk.getCurrentPage(), numberPage);
		return processingURL;
	}
	public void setProcessingURLForTab(String processingURLForTab) {
		this.processingURLForTab = new String(processingURLForTab);
	}
	public String getProcessingURLForTab() {
		return processingURLForTab;
	}
	
	public PortletURL getResultURL() {
		return resultURL;
	}
	public void setResultURL(PortletURL resultURL) {
		this.resultURL = resultURL;
	}
	public PortletURL getResultURL(String numberPage) {
		resultURL.setParameter(AuctionsOmsk.getCurrentPage(), numberPage);
		return resultURL;
	}
	public void setResultURLForTab(String resultURLForTab) {
		this.resultURLForTab = new String(resultURLForTab);
	}
	public String getResultURLForTab() {
		return resultURLForTab;
	}
	
	public void setSearchFromAuction(String searchFromAuction) {
		this.searchFromAuction = searchFromAuction;
	}
	public String getSearchFromAuction() {
		return searchFromAuction;
	}
	public void setSearchToAuction(String searchToAuction) {
		this.searchToAuction = searchToAuction;
	}
	public String getSearchToAuction() {
		return searchToAuction;
	}
	
	public PortletURL getArchiveURL() {
		return archiveURL;
	}
	public void setArchiveURL(PortletURL archiveURL) {
		this.archiveURL = archiveURL;
	}
	public String getCurrentURL() {
		return currentURL;
	}
	public void setCurrentURL(String currentURL) {
		this.currentURL = currentURL;
	}
	public ArrayList<Auction> getAuctionsFiling() {
		return auctionsFiling;
	}
	public void setAuctionsFiling(ArrayList<Auction> auctionsFiling) {
		this.auctionsFiling = auctionsFiling;
	}
	public ArrayList<Auction> getAuctionsProcessing() {
		return auctionsProcessing;
	}
	public void setAuctionsProcessing(ArrayList<Auction> auctionsProcessing) {
		this.auctionsProcessing = auctionsProcessing;
	}
	public ArrayList<Auction> getAuctionsResult() {
		return auctionsResult;
	}
	public void setAuctionsResult(ArrayList<Auction> auctionsResult) {
		this.auctionsResult = auctionsResult;
	}
	public ArrayList<Auction> getAuctionsArchive() {
		return auctionsArchive;
	}
	public void setAuctionsArchive(ArrayList<Auction> auctionsArchive) {
		this.auctionsArchive = auctionsArchive;
	}
	public Paginator getPaginator() {
		return paginator;
	}
	public void setPaginator(Paginator paginator) {
		this.paginator = paginator;
	}
	public ArrayList<Auction> getPageAuctionsArchive() {
		return pageAuctionsArchive;
	}
	public void setPageAuctionsArchive(ArrayList<Auction> pageAuctionsArchive) {
		this.pageAuctionsArchive = pageAuctionsArchive;
	}
	
	/*
	public String getCurrentPageArchive() {
		return currentPageArchive;
	}
	public void setCurrentPageArchive(String currentPageArchive) {
		this.currentPageArchive = currentPageArchive;
	}
	public ArrayList<Pagination> getPaginationPageArchive() {
		return paginationPageArchive;
	}
	public void setPaginationPageArchive(ArrayList<Pagination> paginationPageArchive) {
		this.paginationPageArchive = paginationPageArchive;
	}
	public String getStepPaginationPageArchive() {
		return stepPaginationPageArchive;
	}
	public void setStepPaginationPageArchive(String stepPaginationPageArchive) {
		this.stepPaginationPageArchive = stepPaginationPageArchive;
	}
	
	public boolean getSwitchPaginationArchive() {
		return switchPaginationArchive;
	}
	public void setSwitchPaginationArchive(boolean switchPaginationArchive) {
		this.switchPaginationArchive = switchPaginationArchive;
	}
	public String getUrlNextPageArchive() {
		return urlNextPageArchive;
	}
	public void setUrlNextPageArchive(String urlNextPageArchive) {
		this.urlNextPageArchive = urlNextPageArchive;
	}
	public String getUrlPreviousPageArchive() {
		return urlPreviousPageArchive;
	}
	public void setUrlPreviousPageArchive(String urlPreviousPageArchive) {
		this.urlPreviousPageArchive = urlPreviousPageArchive;
	}
	public String getUrlStartPageArchive() {
		return urlStartPageArchive;
	}
	public void setUrlStartPageArchive(String urlStartPageArchive) {
		this.urlStartPageArchive = urlStartPageArchive;
	}
	public String getUrlEndPageArchive() {
		return urlEndPageArchive;
	}
	public void setUrlEndPageArchive(String urlEndPageArchive) {
		this.urlEndPageArchive = urlEndPageArchive;
	}
	*/
	
	public static boolean isPhotos(Auction auction) {
		boolean is = true;
		for (Objects object : auction.getObjects()) {
			if (object.getPhotos() == null) is = false;
			else {
				if (object.getPhotos().size() <= 0) is = false;
			}
			
		}
		return is;
	}
	public static String getStringMonth(String numMonth) {
		return strMonth[Integer.valueOf(numMonth)-1];
	}
	//склонение по числу, используются 3 состояния
	public static String getDeclination(String number, String state1, String state2, String state3) {
		String result = state3;
		//последний символ
		int lastChar = Integer.valueOf(String.valueOf(number.charAt(number.length() - 1)));
		//предпоследний символ
		int penChar = -1;
		//если больше 1 символа
		if (number.length() > 1) {
			penChar = Integer.valueOf(String.valueOf(number.charAt(number.length() - 2)));
		}
		//если заканчивается на 1, но кроме, если заканчивается на 11 
		if (lastChar == 1 && (penChar== -1 || penChar != 1)) {
			result = state1;
			return result;
		}
		//если заканчивается от 2 до 4 включительно, кроме предпоследнего символа = 1
		if ((lastChar >=2 && lastChar <=4) && (penChar== -1 || penChar != 1)) {
			result = state2;
			return result;
		}
		
		
		return result;
	}
	public static String getSize(String size) {
		
		int newScale = 1;
		Double dSize = Double.valueOf(size);
		if (dSize >= 0.0 && dSize < 1024.0) {
			return size;
		}
		if (dSize >= 1024.0 && dSize < 1024.0*1024.0) {
			double newDouble = new BigDecimal(dSize/1024.0).setScale(newScale, RoundingMode.HALF_UP).doubleValue();
			//целая часть
			double whole = (int)newDouble;
			//дробная часть
			double fract = newDouble - whole;
			if (fract == 0.0) {
				return String.valueOf(whole);
			}
			return String.valueOf(newDouble);
		}
		
		if (dSize >= 1024.0*1024.0 && dSize < 1024.0*1024.0*1024.0) {			
			double newDouble = new BigDecimal(dSize/(1024.0*1024.0)).setScale(newScale, RoundingMode.HALF_UP).doubleValue();
			//целая часть
			double whole = (int)newDouble;
			//дробная часть
			double fract = newDouble - whole;
			if (fract == 0.0) {
				return String.valueOf(whole);
			}
			return String.valueOf(newDouble);
		}
		
		if (dSize >= 1024.0*1024.0*1024.0 && dSize < 1024.0*1024.0*1024.0*1024.0) {
			double newDouble = new BigDecimal(dSize/(1024.0*1024.0*1024.0*1024.0)).setScale(newScale, RoundingMode.HALF_UP).doubleValue();
			//целая часть
			double whole = (int)newDouble;
			//дробная часть
			double fract = newDouble - whole;
			if (fract == 0.0) {
				return String.valueOf(whole);
			}
			return String.valueOf(newDouble);
		}
		
		
		
		return null;
	}
	public static String getMetricSize(String size, String stateB, String stateKb, String stateMb, String stateGb) {
		
		Double dSize = Double.valueOf(size);
		if (dSize >= 0.0 && dSize < 1024.0) {
			return stateB;
		}
		if (dSize >= 1024.0 && dSize < 1024.0*1024.0) {
			return stateKb;
		}
		
		if (dSize >= 1024.0*1024.0 && dSize < 1024.0*1024.0*1024.0) {
			return stateMb;
		}
		
		if (dSize >= 1024.0*1024.0*1024.0 && dSize < 1024.0*1024.0*1024.0*1024.0) {
			return stateGb;
		}
		return null;
	}
	public static String getContextPath() {
		return contextPath;
	}
	public static void setContextPath(String contextPath) {
		ViewBean.contextPath = contextPath;
	}
	
}
