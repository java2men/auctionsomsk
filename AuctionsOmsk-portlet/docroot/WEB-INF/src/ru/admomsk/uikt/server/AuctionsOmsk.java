package ru.admomsk.uikt.server;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.portlet.PortletURLUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.tools.generic.MathTool;

import ru.admomsk.uikt.server.beans.EditBean;
import ru.admomsk.uikt.server.beans.ViewBean;
import ru.admomsk.uikt.server.objects.Auction;
import ru.admomsk.uikt.server.objects.Lot;
import ru.admomsk.uikt.server.objects.Objects;
import ru.admomsk.uikt.server.objects.OneDoc;
import ru.admomsk.uikt.server.objects.Paginator;
import ru.admomsk.uikt.server.objects.Photo;
import ru.admomsk.uikt.utils.MyUtils;
import ru.admomsk.uikt.utils.Pagination;

/**
 * Portlet implementation class AuctionsOmskPortlet
 */
public class AuctionsOmsk extends GenericPortlet {

	//private static boolean  isVelocityEngineStarted = false;
	protected String editJSP;
    protected String viewJSP;
    //параметры запроса
    protected static String URL_STAGE = "url_stage";
	protected static String CURRENT_PAGE = "current_page";
	protected static String SEARCH_AUCTIONS = "search_auctions";
	protected static String SEARCH_FROM_AUCTION = "search_from_auction";
	protected static String SEARCH_TO_AUCTION = "search_to_auction";
    //protected static String URL_ARCHIVE = "url_archive";
    protected VelocityContext velocityContext;
    private static Log _log = LogFactoryUtil.getLog(AuctionsOmsk.class);
	
    public static String getURLStage() {
		return URL_STAGE;
	}
	public static String getCurrentPage() {
		return CURRENT_PAGE;
	}
	public static String getSearchAuctions() {
		return SEARCH_AUCTIONS;
	}
	public static String getFromSearchAuction() {
		return SEARCH_FROM_AUCTION;
	}
	public static String getToSearchAuction() {
		return SEARCH_TO_AUCTION;
	}
	
    public void init() {
        editJSP = getInitParameter("edit-jsp");
        viewJSP = getInitParameter("view-jsp");
    }

    public void processAction(
            ActionRequest actionRequest, ActionResponse actionResponse)
        throws IOException, PortletException {
    	
    	String portletName = (String)actionRequest.getAttribute(WebKeys.PORTLET_ID);
    	ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	PortletURL redirectURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest),portletName,
    	themeDisplay.getLayout().getPlid(), PortletRequest.RENDER_PHASE);
    	
		redirectURL.setParameter(URL_STAGE, actionRequest.getParameter(URL_STAGE));
		redirectURL.setParameter(CURRENT_PAGE, "1");
		redirectURL.setParameter(SEARCH_AUCTIONS, actionRequest.getParameter(SEARCH_AUCTIONS));
		redirectURL.setParameter(SEARCH_FROM_AUCTION, actionRequest.getParameter(SEARCH_FROM_AUCTION));
		redirectURL.setParameter(SEARCH_TO_AUCTION, actionRequest.getParameter(SEARCH_TO_AUCTION));
    	
    	actionResponse.sendRedirect(redirectURL.toString()); 
        //super.processAction(actionRequest, actionResponse);
    }
    
    public void doEdit(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
    	
    	PortletSession portletSession = renderRequest.getPortletSession();
    	EditBean editBean = (EditBean)portletSession.getAttribute("editBean");
    	
    	//если EditBean не создан, то создать его
    	if (editBean == null){
    		editBean = new EditBean();
    		//Установить настройки
    		editBean.setPortletPreferences(renderRequest.getPreferences());
    		
    		portletSession.setAttribute("editBean", editBean);
    	}
		
		//Если произошел запрос 
		if (renderRequest.getParameterMap().size() > 0) {
    		editBean.setRequestFilingURL(renderRequest.getParameter("filing_url"));
    		editBean.setRequestProcessingURL(renderRequest.getParameter("processing_url"));
    		editBean.setRequestResultURL(renderRequest.getParameter("result_url"));
    		//editBean.setRequestArchiveURL(renderRequest.getParameter("archive_url"));
    		//System.out.println("renderRequest.getParameter(\"type_auctions\") = " + renderRequest.getParameter("type_auctions"));
    		//editBean.setTypeAuctions(renderRequest.getParameter("type_auctions"));
    		//editBean.setCountArchiveAuctions(renderRequest.getParameter("count_archive_auctions"));
    		//System.out.println("editBean.isValidAll() = " + editBean.isValidAll()); 
    	}
    	
    	HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		/** initialize Velocity Engine */
		velocityInit(httpRequest);
		velocityContext = new VelocityContext();
		
		velocityContext.put("editBean", editBean);
		velocityContext.put("editRenderURL", renderResponse.createRenderURL());
		renderRequest.setAttribute("templateOutput", writeIntoVelocity(getInitParameter("edit-vm"), velocityContext));
        include(editJSP, renderRequest, renderResponse);
    }
    
    public void doView(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
    	
    	//ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	//ViewBean.instanceId = themeDisplay.getPortletDisplay().getInstanceId();
    	ViewBean.instanceId = String.valueOf(this.hashCode());
    	
    	PortletURL renderURL;
    	PortletURL actionURL;
    	PortletSession portletSession = renderRequest.getPortletSession();
    	ViewBean viewBean = (ViewBean)portletSession.getAttribute("viewBean");
    	EditBean editBean = (EditBean)portletSession.getAttribute("editBean");
    	
    	//установить ContextPath для поиска js, css...
    	ViewBean.setContextPath(renderRequest.getContextPath());
    	EditBean.setContextPath(renderRequest.getContextPath());
    	
    	//viewBean.setRenderRequest(renderRequest);
    	
    	
    	//если ViewBean не создан, то создать его
    	if (viewBean == null) {
    		viewBean = new ViewBean();
    		
    		renderURL = renderResponse.createRenderURL();
    		renderURL.setParameter(URL_STAGE, ViewBean.getFilingValue());
    		renderURL.setParameter(CURRENT_PAGE, "1");
    		renderURL.setParameter(SEARCH_AUCTIONS, "false");
    		renderURL.setParameter(SEARCH_FROM_AUCTION, "null");
    		renderURL.setParameter(SEARCH_TO_AUCTION, "null");
    		System.out.println("renderURL.toString() = " +  renderURL.toString());
    		viewBean.setFilingURL(renderURL);
    		viewBean.setFilingURLForTab(renderURL.toString());
    		System.out.println("viewBean.getFilingURL().toString() = " +  viewBean.getFilingURL().toString());
    		actionURL = renderResponse.createActionURL();
			actionURL.setParameter(URL_STAGE, ViewBean.getFilingValue());
			actionURL.setParameter(CURRENT_PAGE, "1");
			actionURL.setParameter(SEARCH_AUCTIONS, "true");
			viewBean.setFilingActionURL(actionURL);
    		
    		
    		renderURL = renderResponse.createRenderURL();
    		renderURL.setParameter(URL_STAGE, ViewBean.getProcessingValue());
    		renderURL.setParameter(CURRENT_PAGE, "1");
    		renderURL.setParameter(SEARCH_AUCTIONS, "false");
    		renderURL.setParameter(SEARCH_FROM_AUCTION, "null");
    		renderURL.setParameter(SEARCH_TO_AUCTION, "null");
    		System.out.println("renderURL.toString() = " +  renderURL.toString());
    		viewBean.setProcessingURL(renderURL);
    		viewBean.setProcessingURLForTab(renderURL.toString());
    		System.out.println("viewBean.getProcessingURL().toString() = " +  viewBean.getProcessingURL().toString());
    		actionURL = renderResponse.createActionURL();
			actionURL.setParameter(URL_STAGE, ViewBean.getProcessingValue());
			actionURL.setParameter(CURRENT_PAGE, "1");
			actionURL.setParameter(SEARCH_AUCTIONS, "true");
			viewBean.setProcessingActionURL(actionURL);
			System.out.println("test1: viewBean.getProcessingActionURL() = " + viewBean.getProcessingActionURL());
			
    		renderURL = renderResponse.createRenderURL();
    		renderURL.setParameter(URL_STAGE, ViewBean.getResultValue());
    		renderURL.setParameter(CURRENT_PAGE, "1");
    		renderURL.setParameter(SEARCH_AUCTIONS, "false");
    		renderURL.setParameter(SEARCH_FROM_AUCTION, "null");
    		renderURL.setParameter(SEARCH_TO_AUCTION, "null");
    		System.out.println("renderURL.toString() = " +  renderURL.toString());
    		viewBean.setResultURL(renderURL);
    		viewBean.setResultURLForTab(renderURL.toString());
    		System.out.println("viewBean.getResultURL().toString() = " +  viewBean.getResultURL().toString());
    		actionURL = renderResponse.createActionURL();
			actionURL.setParameter(URL_STAGE, ViewBean.getResultValue());
			actionURL.setParameter(CURRENT_PAGE, "1");
			actionURL.setParameter(SEARCH_AUCTIONS, "true");
			viewBean.setResultActionURL(actionURL);
    		
    		//Установить настройки
    		portletSession.setAttribute("viewBean", viewBean);
    	}
    	
    	viewBean.setCurrentURL("");
    	
    	//если EditBean не создан, то создать его
    	if (editBean == null){
    		editBean = new EditBean();
    		//Установить настройки
    		editBean.setPortletPreferences(renderRequest.getPreferences());
    		
    		portletSession.setAttribute("editBean", editBean);
    	}
    	
    	
    	//Если произошел запрос 
		if (renderRequest.getParameterMap().size() > 0) {
			
			if ( renderRequest.getParameter(URL_STAGE).equals(ViewBean.getFilingValue()) ) {
				
				actionURL = renderResponse.createActionURL();
				actionURL.setParameter(URL_STAGE, ViewBean.getFilingValue());
				actionURL.setParameter(CURRENT_PAGE, "1");
				actionURL.setParameter(SEARCH_AUCTIONS, "true");
				viewBean.setFilingActionURL(actionURL);
				
				//Обновить portletURL
				viewBean.setFilingURL(PortletURLUtil.getCurrent(renderRequest, renderResponse));
				
				if (editBean.getValidateRequestFilingURL() != EditBean.getBadURL()) {
					viewBean.getFilingURL().setParameter(CURRENT_PAGE, renderRequest.getParameter(CURRENT_PAGE));
					JSONObject jsonObject = null;
					
					if (renderRequest.getParameter(SEARCH_AUCTIONS).equals("true")) {
						
						viewBean.getFilingURL().setParameter(SEARCH_AUCTIONS, "true");
						System.out.println("renderRequest.getParameter(FROM_SEARCH_AUCTION) = " + renderRequest.getParameter(SEARCH_FROM_AUCTION)); 
						viewBean.getFilingURL().setParameter(SEARCH_FROM_AUCTION, renderRequest.getParameter(SEARCH_FROM_AUCTION));
						viewBean.getFilingURL().setParameter(SEARCH_TO_AUCTION, renderRequest.getParameter(SEARCH_TO_AUCTION));
						
						viewBean.setSearchFromAuction(renderRequest.getParameter(SEARCH_FROM_AUCTION));
						viewBean.setSearchToAuction(renderRequest.getParameter(SEARCH_TO_AUCTION));
						
						String from = MyUtils.convertSDF(renderRequest.getParameter(SEARCH_FROM_AUCTION),  new SimpleDateFormat("dd.MM.yyyy"), new SimpleDateFormat("yyyy-MM-dd"));
		    			String to = MyUtils.convertSDF(renderRequest.getParameter(SEARCH_TO_AUCTION), new SimpleDateFormat("dd.MM.yyyy"), new SimpleDateFormat("yyyy-MM-dd"));
		    			if (from == null) from = renderRequest.getParameter(SEARCH_FROM_AUCTION);
		    			if (to == null) to = renderRequest.getParameter(SEARCH_TO_AUCTION); 				    			
						
						jsonObject = requestServiceJSON(editBean.getRequestFilingURL(renderRequest.getParameter(CURRENT_PAGE), from, to), "10.0.0.1", "8080", "ialozhnikov", "ialoz");

					} else {
						viewBean.getFilingURL().setParameter(SEARCH_AUCTIONS, "false");
						viewBean.getFilingURL().setParameter(SEARCH_FROM_AUCTION, "null");
						viewBean.getFilingURL().setParameter(SEARCH_TO_AUCTION, "null");
						
						viewBean.setSearchFromAuction("");
						viewBean.setSearchToAuction("");
						
						jsonObject = requestServiceJSON(editBean.getRequestFilingURL(renderRequest.getParameter(CURRENT_PAGE)), "10.0.0.1", "8080", "ialozhnikov", "ialoz");
					}
					
					if (jsonObject != null) {
						viewBean.setAuctionsFiling(getListAuctions(jsonObject));
						viewBean.setPaginator(getPaginator(jsonObject));
					} else {
						viewBean.setAuctionsFiling(new ArrayList<Auction>());
					}
				}
				
				viewBean.setCurrentURL(viewBean.getFilingURL().toString());
			}
			if (renderRequest.getParameter(URL_STAGE).equals(ViewBean.getProcessingValue())) {
				
				actionURL = renderResponse.createActionURL();
				actionURL.setParameter(URL_STAGE, ViewBean.getProcessingValue());
				actionURL.setParameter(CURRENT_PAGE, "1");
				actionURL.setParameter(SEARCH_AUCTIONS, "true");
				viewBean.setProcessingActionURL(actionURL);
				
				viewBean.setProcessingURL(PortletURLUtil.getCurrent(renderRequest, renderResponse));
				
				if (editBean.getValidateRequestProcessingURL() != EditBean.getBadURL()) {
					
					viewBean.getProcessingURL().setParameter(CURRENT_PAGE, renderRequest.getParameter(CURRENT_PAGE));
					JSONObject jsonObject = null;
					
					if (renderRequest.getParameter(SEARCH_AUCTIONS).equals("true")) {
						viewBean.getProcessingURL().setParameter(SEARCH_AUCTIONS, "true"); 
						viewBean.getProcessingURL().setParameter(SEARCH_FROM_AUCTION, renderRequest.getParameter(SEARCH_FROM_AUCTION));
						viewBean.getProcessingURL().setParameter(SEARCH_TO_AUCTION, renderRequest.getParameter(SEARCH_TO_AUCTION));
						
						viewBean.setSearchFromAuction(renderRequest.getParameter(SEARCH_FROM_AUCTION));
						viewBean.setSearchToAuction(renderRequest.getParameter(SEARCH_TO_AUCTION));
						
						String from = MyUtils.convertSDF(renderRequest.getParameter(SEARCH_FROM_AUCTION),  new SimpleDateFormat("dd.MM.yyyy"), new SimpleDateFormat("yyyy-MM-dd"));
		    			String to = MyUtils.convertSDF(renderRequest.getParameter(SEARCH_TO_AUCTION), new SimpleDateFormat("dd.MM.yyyy"), new SimpleDateFormat("yyyy-MM-dd"));
		    			if (from == null) from = renderRequest.getParameter(SEARCH_FROM_AUCTION);
		    			if (to == null) to = renderRequest.getParameter(SEARCH_TO_AUCTION); 	
						
						jsonObject = requestServiceJSON(editBean.getRequestProcessingURL(renderRequest.getParameter(CURRENT_PAGE), from, to), "10.0.0.1", "8080", "ialozhnikov", "ialoz");
						
					} else {
						viewBean.getProcessingURL().setParameter(SEARCH_AUCTIONS, "false");
						viewBean.getProcessingURL().setParameter(SEARCH_FROM_AUCTION, "null");
						viewBean.getProcessingURL().setParameter(SEARCH_TO_AUCTION, "null");
						
						viewBean.setSearchFromAuction("");
						viewBean.setSearchToAuction("");
						
						jsonObject = requestServiceJSON(editBean.getRequestProcessingURL(renderRequest.getParameter(CURRENT_PAGE)), "10.0.0.1", "8080", "ialozhnikov", "ialoz");
					}
					
					if (jsonObject != null) {
						viewBean.setAuctionsProcessing(getListAuctions(jsonObject));
						viewBean.setPaginator(getPaginator(jsonObject));
					} else {
						viewBean.setAuctionsProcessing(new ArrayList<Auction>());
					}
					
				}
				
				viewBean.setCurrentURL(viewBean.getProcessingURL().toString());
			}
			if (renderRequest.getParameter(URL_STAGE).equals(ViewBean.getResultValue())) {
				actionURL = renderResponse.createActionURL();
				actionURL.setParameter(URL_STAGE, ViewBean.getResultValue());
				actionURL.setParameter(CURRENT_PAGE, "1");
				actionURL.setParameter(SEARCH_AUCTIONS, "true");
				viewBean.setResultActionURL(actionURL);

				viewBean.setResultURL(PortletURLUtil.getCurrent(renderRequest, renderResponse));
				
				if (editBean.getValidateRequestResultURL() != EditBean.getBadURL()) {
					
					viewBean.getResultURL().setParameter(CURRENT_PAGE, renderRequest.getParameter(CURRENT_PAGE));
					JSONObject jsonObject = null;
					
					if (renderRequest.getParameter(SEARCH_AUCTIONS).equals("true")) {
						viewBean.getResultURL().setParameter(SEARCH_AUCTIONS, "true"); 
						viewBean.getResultURL().setParameter(SEARCH_FROM_AUCTION, renderRequest.getParameter(SEARCH_FROM_AUCTION));
						viewBean.getResultURL().setParameter(SEARCH_TO_AUCTION, renderRequest.getParameter(SEARCH_TO_AUCTION));
						
						viewBean.setSearchFromAuction(renderRequest.getParameter(SEARCH_FROM_AUCTION));
						viewBean.setSearchToAuction(renderRequest.getParameter(SEARCH_TO_AUCTION));
						
						String from = MyUtils.convertSDF(renderRequest.getParameter(SEARCH_FROM_AUCTION),  new SimpleDateFormat("dd.MM.yyyy"), new SimpleDateFormat("yyyy-MM-dd"));
		    			String to = MyUtils.convertSDF(renderRequest.getParameter(SEARCH_TO_AUCTION), new SimpleDateFormat("dd.MM.yyyy"), new SimpleDateFormat("yyyy-MM-dd"));
		    			if (from == null) from = renderRequest.getParameter(SEARCH_FROM_AUCTION);
		    			if (to == null) to = renderRequest.getParameter(SEARCH_TO_AUCTION);
						
						jsonObject = requestServiceJSON(editBean.getRequestResultURL(renderRequest.getParameter(CURRENT_PAGE), from, to), "10.0.0.1", "8080", "ialozhnikov", "ialoz");
						
					}
					else {
						viewBean.getResultURL().setParameter(SEARCH_AUCTIONS, "false");
						viewBean.getResultURL().setParameter(SEARCH_FROM_AUCTION, "null");
						viewBean.getResultURL().setParameter(SEARCH_TO_AUCTION, "null");
						
						viewBean.setSearchFromAuction("");
						viewBean.setSearchToAuction("");
						
						jsonObject = requestServiceJSON(editBean.getRequestResultURL(renderRequest.getParameter(CURRENT_PAGE)), "10.0.0.1", "8080", "ialozhnikov", "ialoz");
					}
					if (jsonObject != null) {
						viewBean.setAuctionsResult(getListAuctions(jsonObject));
						viewBean.setPaginator(getPaginator(jsonObject));
					}
					else {
						viewBean.setAuctionsResult(new ArrayList<Auction>());
					}
					
				}
				
				viewBean.setCurrentURL(viewBean.getResultURL().toString());
			}
			
    	} else {
    		
    		viewBean.setSearchFromAuction("");
			viewBean.setSearchToAuction("");
    		renderURL = PortletURLUtil.getCurrent(renderRequest, renderResponse);
    		renderURL.setParameter(URL_STAGE, ViewBean.getFilingValue());
    		renderURL.setParameter(CURRENT_PAGE, "1");
    		renderURL.setParameter(SEARCH_AUCTIONS, "false");
    		renderURL.setParameter(SEARCH_FROM_AUCTION, "null");
    		renderURL.setParameter(SEARCH_TO_AUCTION, "null");
    		//System.out.println("renderURL.toString() = " +  renderURL.toString());
    		viewBean.setFilingURL(renderURL);
    		
			if (editBean.getValidateRequestFilingURL() != EditBean.getBadURL()) {
				
				JSONObject jsonObject = null;
				jsonObject = requestServiceJSON(editBean.getRequestFilingURL("1"), "10.0.0.1", "8080", "ialozhnikov", "ialoz");
				if (jsonObject != null) {
					viewBean.setAuctionsFiling(getListAuctions(jsonObject));
					viewBean.setPaginator(getPaginator(jsonObject));
				}
				else {
					viewBean.setAuctionsFiling(new ArrayList<Auction>());
				}
				
				viewBean.setCurrentURL(viewBean.getFilingURL().toString());
			}
			
    	}
		
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		/** initialize Velocity Engine */
		velocityInit(httpRequest);
		velocityContext = new VelocityContext();
		velocityContext.put("math", new MathTool());
		velocityContext.put("viewBean", viewBean);
		velocityContext.put("editBean", editBean);
		
		renderRequest.setAttribute("templateOutput", writeIntoVelocity(getInitParameter("view-vm"), velocityContext));
        include(viewJSP, renderRequest, renderResponse);
    }

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
    
    public void velocityInit(HttpServletRequest httpRequest) {
    	try {
				/** initialize Velocity Engine */
				Velocity.setProperty("resource.loader", "webapp");
				Velocity.setProperty("webapp.resource.loader.class", "org.apache.velocity.tools.view.servlet.WebappLoader");
				Velocity.setApplicationAttribute("javax.servlet.ServletContext",httpRequest.getSession().getServletContext());
				Velocity.init();
				//Template template = Velocity.getTemplate("subject.vm", "UTF-8");
		} catch (Exception e) {
			//isVelocityEngineStarted = false;
			_log.error("Problem initializing Velocity : " + e);
		}
    }
    
	@SuppressWarnings("finally")
	public String writeIntoVelocity(String templateUrl, VelocityContext velocityContext) {
		Writer writer = new StringWriter();
		try {
			_log.info("templateUrl: " +templateUrl);        
			Template template = Velocity.getTemplate(templateUrl, "UTF-8");
			/* Template merging */
			template.merge(velocityContext, writer);
		} catch (Exception e) {
			_log.error("Problem merging template, "+ templateUrl);
			_log.error(e);
			//e.printStackTrace();
		} finally {
			//System.out.println("writer.toString() = " + writer.toString());
			return writer.toString();
		} 
		
		
	}
	
	//с прокси
	@SuppressWarnings("finally")
	protected JSONObject requestServiceJSON(String url, String proxyHost, String proxyPort, String username, String password) {
		// Create an instance of HttpClient.
		HttpClient client = new HttpClient();
		JSONObject jsonObj = null;
		BufferedReader br = null;
		
		
		/*if ((proxyHost != null) && (proxyPort != null)) {
			if (!proxyHost.equals("") && !proxyPort.equals("")) {
				AuthScope authScope = new AuthScope(proxyHost, new Integer(proxyPort).intValue(), AuthScope.ANY_REALM);
				//System.out.println("authScope.getHost() = " + authScope.getHost());
				//System.out.println("authScope.getPort() = " + authScope.getPort());
				if (username == null) username = "";
				if (password == null) password = "";
				UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(username, password);
				//System.out.println("usernamePasswordCredentials.getUserName() = " + usernamePasswordCredentials.getUserName());
				//System.out.println("usernamePasswordCredentials.getPassword() = " + usernamePasswordCredentials.getPassword());
				HttpState state = new HttpState();
				state.setProxyCredentials(authScope, usernamePasswordCredentials);
				client.getHostConfiguration().setProxy(authScope.getHost(), authScope.getPort());
				client.setState(state);
				//System.out.println("я зашел сюда");
			}
		}*/
		
		
		GetMethod method = new GetMethod();
		
		try {
			//method = new GetMethod(new String(url.getBytes(),"UTF-8"));
			method.setURI(new URI(url, false, "UTF-8"));
		} catch (URIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
				new DefaultHttpMethodRetryHandler(3, false));
		
		try {
			// Create a method instance.
			//method = new GetMethod(url);
			//method = new GetMethod(new String(url.getBytes(),"UTF-8"));
			//method = new GetMethod(URLEncoder.encode(url, "UTF-8"));
			//System.out.println("URLEncoder.encode(url, \"UTF-8\") = " + new String(url.getBytes(),"UTF-8"));
			
			// Execute the method.
			int statusCode = client.executeMethod(method);
			
			if (statusCode != HttpStatus.SC_OK) {
				_log.error("Method failed: " + method.getStatusLine());
			}
			// Read the response body.
			//byte[] responseBody = method.getResponseBody();
			// Deal with the response.
			// Use caution: ensure correct character encoding and is not binary data
			//String json = new String(responseBody);
			br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
			String readLine;
			StringBuffer sbJson = new StringBuffer();
			while(((readLine = br.readLine()) != null)) {
				//System.err.println(readLine);
				sbJson.append(readLine);
			}
			jsonObj = JSONFactoryUtil.createJSONObject(sbJson.toString());
		
		} catch (HttpException e) {
			//System.err.println("Fatal protocol violation: " + e.getMessage());
			_log.error("Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			//System.err.println("Fatal transport error: " + e.getMessage());
			_log.error("Fatal transport error: " + e.getMessage());
		} catch (JSONException e) {
			//System.err.println("Json object error: " + e.getMessage());
			_log.error("Json object error: " + e.getMessage());
		} finally {
			// Release the connection.
			method.releaseConnection();
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return jsonObj;
		}
	}
	
	//без прокси
	@SuppressWarnings("finally")
	protected JSONObject requestServiceJSON(String url) {
		// Create an instance of HttpClient.
		HttpClient client = new HttpClient();
		JSONObject jsonObj = null;
		BufferedReader br = null;
		//client.getHostConfiguration().setProxy("10.0.0.1", 8080);
		//client.getHostConfiguration().
		
		/*client.getCredentialsProvider().setCredentials(
		        new AuthScope(targetHost.getHostName(), targetHost.getPort()),
		        new UsernamePasswordCredentials("test", "test"));*/
		
		HttpState state = new HttpState();
		client.setState(state);
		
		// Create a method instance.
		GetMethod method = new GetMethod(url);
		
		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
				new DefaultHttpMethodRetryHandler(3, false));
		
		try {
			// Execute the method.
			int statusCode = client.executeMethod(method);
			
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println();
				_log.error("Method failed: " + method.getStatusLine());
			}
			
			br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
			String readLine;
			StringBuffer sbJson = new StringBuffer();
			while(((readLine = br.readLine()) != null)) {
				//System.err.println(readLine);
				sbJson.append(readLine);
			}
			jsonObj = JSONFactoryUtil.createJSONObject(sbJson.toString());
		
		} catch (HttpException e) {
			System.err.println("Fatal protocol violation: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Fatal transport error: " + e.getMessage());
			e.printStackTrace();
		} finally {
			// Release the connection.
			method.releaseConnection();
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return jsonObj;
		}
	}
	
	protected ArrayList<Auction> getListAuctions(JSONObject jsonObject) {
		ArrayList<Auction> listAuctions = new ArrayList<Auction>();
		JSONArray jaAuctions = jsonObject.getJSONArray("auctions");
		for (int i = 0; i < jaAuctions.length(); i++) {
			//System.out.println("date = " + jaAuctions.getJSONObject(i).getString("date")) ;
			Auction auction = new Auction();
			auction.setId("a" + String.valueOf(i));
			auction.setDay(jaAuctions.getJSONObject(i).getString("day"));
			auction.setMonth(jaAuctions.getJSONObject(i).getString("month"));
			auction.setYear(jaAuctions.getJSONObject(i).getString("year"));
			auction.setLots(jaAuctions.getJSONObject(i).getString("lots"));
			auction.setOnedocs(new ArrayList<OneDoc>());
			auction.setObjects(new ArrayList<Objects>());
			JSONArray jaOnedocs = jaAuctions.getJSONObject(i).getJSONArray("onedocs");
			//auction.setDate(date)
			for (int j = 0; j < jaOnedocs.length(); j++) {
				OneDoc oneDoc = new OneDoc();
				oneDoc.setName(jaOnedocs.getJSONObject(j).getString("name"));
				oneDoc.setUrl(jaOnedocs.getJSONObject(j).getString("url"));
				oneDoc.setType(jaOnedocs.getJSONObject(j).getString("type"));
				oneDoc.setSize(jaOnedocs.getJSONObject(j).getString("size"));
				oneDoc.setDescription(jaOnedocs.getJSONObject(j).getString("description"));
				oneDoc.setDownloads(jaOnedocs.getJSONObject(j).getString("downloads"));
				oneDoc.setDayPublished(jaOnedocs.getJSONObject(j).getString("dayPublished"));
				oneDoc.setMonthPublished(jaOnedocs.getJSONObject(j).getString("monthPublished"));
				oneDoc.setYearPublished(jaOnedocs.getJSONObject(j).getString("yearPublished"));
				auction.getOnedocs().add(oneDoc);
			}
			JSONArray jaObjects = jaAuctions.getJSONObject(i).getJSONArray("objects");
			//auction.setDate(date)
			for (int o = 0; o < jaObjects.length(); o++) {
				Objects object = new Objects();
				object.setId("a" + String.valueOf(i) + "o"+ String.valueOf(o));
				object.setLots(new ArrayList<Lot>());
				object.setName(jaObjects.getJSONObject(o).getString("name"));
				object.setLng(jaObjects.getJSONObject(o).getString("lng"));
				object.setLat(jaObjects.getJSONObject(o).getString("lat"));
				
				JSONArray jaLots = jaObjects.getJSONObject(o).getJSONArray("lots");
				for (int l = 0; l < jaLots.length(); l++) {
					Lot lot = new Lot();
					lot.setName(jaLots.getJSONObject(l).getString("name"));
					lot.setDescription(jaLots.getJSONObject(l).getString("description"));
					object.getLots().add(lot);
				}
				
				object.setPhotos(new ArrayList<Photo>());
				JSONArray jaPhotos = jaObjects.getJSONObject(o).getJSONArray("photos");
				for (int p = 0; p < jaPhotos.length(); p++) {
					Photo photo = new Photo();
					photo.setDescription(jaPhotos.getJSONObject(p).getString("description"));
					photo.setUrl(jaPhotos.getJSONObject(p).getString("url"));
					object.getPhotos().add(photo);
				}
				
				auction.getObjects().add(object);
			}
			listAuctions.add(auction);
		}
		return listAuctions;
	}
	
	protected Paginator getPaginator(JSONObject jsonObject) {
		Paginator paginator = new Paginator();
		//JSONArray jaPaginator = jsonObject.getJSONArray("paginator");
		JSONObject joPaginator = jsonObject.getJSONObject("paginator");
		paginator.setTotal(joPaginator.getString("total"));
		paginator.setCurrent(joPaginator.getString("current"));
		paginator.setOffset(joPaginator.getString("offset"));
		paginator.setRows(joPaginator.getString("rows"));
		return paginator;
	}
	
}
