package ru.admomsk.uikt.server.objects;

import java.util.ArrayList;


public class Objects {
	protected String id;
	protected String name;
	protected ArrayList<Lot> lots = new ArrayList<Lot>();
	protected ArrayList<Photo> photos = new ArrayList<Photo>();
	protected String lng;
	protected String lat;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Lot> getLots() {
		return lots;
	}
	public void setLots(ArrayList<Lot> lots) {
		this.lots = lots;
	}
	public ArrayList<Photo> getPhotos() {
		return photos;
	}
	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	
}
