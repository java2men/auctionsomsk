package ru.admomsk.uikt.server.beans;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.ValidatorException;

public class EditBean implements Serializable {
	
	private static final long serialVersionUID = 8082776350473718379L;
	public static String currentTypeAuctions = "current";
	public static String archiveTypeAuctions = "archive";
	protected final static String badRange = "bad_range";
	protected final static String badNumber = "bad_number";
	protected final static String badURL = "bad_url";
	protected final static String idle = "idle";
	protected final static String ok = "ok";
	
	protected RenderRequest renderRequest;
	protected PortletPreferences portletPreferences;
	protected String requestFilingURL = "";
	protected String requestProcessingURL = "";
	protected String requestResultURL = "";
	/*protected String requestArchiveURL = "";
	protected String typeAuctions = "";
	protected String countArchiveAuctions = "";*/
	
	protected String validateRequestFilingURL;
	protected String validateRequestProcessingURL;
	protected String validateRequestResultURL;
	/*protected String validateRequestArchiveURL;
	protected String validateCountArchiveAuctions;*/
	protected boolean validAll;
	
	protected static String contextPath;
	/*public EditBean(RenderRequest renderRequest) {
		this.renderRequest = renderRequest;
		this.portletPreferences = this.renderRequest.getPreferences();
	}*/
	
	public PortletPreferences getPortletPreferences() {
		return portletPreferences;
	}
	public void setPortletPreferences(PortletPreferences portletPreferences) {
		this.portletPreferences = portletPreferences;
	}
	public String getRequestFilingURL() {
		requestFilingURL = (String)portletPreferences.getValue("filingURL", "");
		return requestFilingURL;
	}
	public String getRequestFilingURL(String currentPage) {
		String countRowsInPage = "10";
		requestFilingURL = (String)portletPreferences.getValue("filingURL", "");
		
		String limit = String.valueOf(Integer.valueOf(countRowsInPage)*(Integer.valueOf(currentPage) - 1));
		return requestFilingURL.replace("?", "/" + limit + "?");
	}
	public String getRequestFilingURL(String currentPage, String dateFrom, String dateTo) {
		String countRowsInPage = "10";
		requestFilingURL = (String)portletPreferences.getValue("filingURL", "");
		String limit = String.valueOf(Integer.valueOf(countRowsInPage)*(Integer.valueOf(currentPage) - 1));
		return requestFilingURL.replace("?", "/" + limit + "?") + "&date_from=" + dateFrom + "&date_to=" + dateTo;
	}
	public void setRequestFilingURL(String requestFilingURL) throws ReadOnlyException, ValidatorException, IOException {
		this.requestFilingURL = requestFilingURL;
		portletPreferences.setValue("filingURL", this.requestFilingURL);
		portletPreferences.store();
	}
	
	public String getRequestProcessingURL() {
		requestProcessingURL = (String)portletPreferences.getValue("processingURL", "");
		return requestProcessingURL;
	}
	public String getRequestProcessingURL(String currentPage) {
		String countRowsInPage = "10";
		requestProcessingURL = (String)portletPreferences.getValue("processingURL", "");
		
		String limit = String.valueOf(Integer.valueOf(countRowsInPage)*(Integer.valueOf(currentPage) - 1));
		return requestProcessingURL.replace("?", "/" + limit + "?");
	}
	public String getRequestProcessingURL(String currentPage, String dateFrom, String dateTo) {
		String countRowsInPage = "10";
		requestProcessingURL = (String)portletPreferences.getValue("processingURL", "");
		String limit = String.valueOf(Integer.valueOf(countRowsInPage)*(Integer.valueOf(currentPage) - 1));
		return requestProcessingURL.replace("?", "/" + limit + "?") + "&date_from=" + dateFrom + "&date_to=" + dateTo;
	}
	public void setRequestProcessingURL(String requestProcessingURL) throws ReadOnlyException, ValidatorException, IOException {
		this.requestProcessingURL = requestProcessingURL;
		portletPreferences.setValue("processingURL", this.requestProcessingURL);
		portletPreferences.store();
	}
	
	public String getRequestResultURL() {
		requestResultURL = (String)portletPreferences.getValue("resultURL", "");
		return requestResultURL;
	}
	public String getRequestResultURL(String currentPage) {
		String countRowsInPage = "10";
		requestResultURL = (String)portletPreferences.getValue("resultURL", "");
		
		String limit = String.valueOf(Integer.valueOf(countRowsInPage)*(Integer.valueOf(currentPage) - 1));
		return requestResultURL.replace("?", "/" + limit + "?");
	}
	public String getRequestResultURL(String currentPage, String dateFrom, String dateTo) {
		String countRowsInPage = "10";
		requestResultURL = (String)portletPreferences.getValue("resultURL", "");
		String limit = String.valueOf(Integer.valueOf(countRowsInPage)*(Integer.valueOf(currentPage) - 1));
		return requestResultURL.replace("?", "/" + limit + "?") + "&date_from=" + dateFrom + "&date_to=" + dateTo;
	}
	public void setRequestResultURL(String requestResultURL) throws ReadOnlyException, ValidatorException, IOException {
		this.requestResultURL = requestResultURL;
		portletPreferences.setValue("resultURL", this.requestResultURL);
		portletPreferences.store();
	}
	
	/*
	public String getRequestArchiveURL() {
		requestArchiveURL = (String)portletPreferences.getValue("archiveURL", "");
		return requestArchiveURL;
	}
	public void setRequestArchiveURL(String requestArchiveURL) throws ReadOnlyException, ValidatorException, IOException {
		this.requestArchiveURL = requestArchiveURL;
		portletPreferences.setValue("archiveURL", this.requestArchiveURL);
		portletPreferences.store();
	}
	public String getTypeAuctions() {
		typeAuctions = (String)portletPreferences.getValue("typeAuctions", "");
		return typeAuctions;
	}
	public void setTypeAuctions(String typeAuctions) throws ReadOnlyException, ValidatorException, IOException {
		this.typeAuctions = typeAuctions;
		portletPreferences.setValue("typeAuctions", this.typeAuctions);
		portletPreferences.store();
	}
	public String getCountArchiveAuctions() {
		countArchiveAuctions = (String)portletPreferences.getValue("countArchiveAuctions", "");
		return countArchiveAuctions;
	}
	public void setCountArchiveAuctions(String countArchiveAuctions) throws ReadOnlyException, ValidatorException, IOException {
		this.countArchiveAuctions = countArchiveAuctions;
		portletPreferences.setValue("countArchiveAuctions", this.countArchiveAuctions);
		portletPreferences.store();
	}*/
	
	@SuppressWarnings("finally")
	public String getValidateRequestFilingURL() {
		validateRequestFilingURL = ok;
		
		if (getRequestFilingURL() != null) {
			if (getRequestFilingURL().equals("")) {
				validateRequestFilingURL = idle;
				return validateRequestFilingURL;
			}
		} else {
			validateRequestFilingURL = idle;
			return validateRequestFilingURL;
		}
		
		try {
			new URL(getRequestFilingURL());
		} catch (MalformedURLException e) {
			validateRequestFilingURL = badURL;
			e.printStackTrace();
		} finally {
			return validateRequestFilingURL;
		}
	}
	@SuppressWarnings("finally")
	public String getValidateRequestProcessingURL() {
		validateRequestProcessingURL = ok;
		
		if (getRequestProcessingURL() != null) {
			if (getRequestProcessingURL().equals("")) {
				validateRequestProcessingURL = idle;
				return validateRequestProcessingURL;
			}
		} else {
			validateRequestProcessingURL = idle;
			return validateRequestProcessingURL;
		}
		
		try {
			new URL(getRequestProcessingURL());
		} catch (MalformedURLException e) {
			validateRequestProcessingURL = badURL;
			e.printStackTrace();
		} finally {
			return validateRequestProcessingURL;
		}
	}
	@SuppressWarnings("finally")
	public String getValidateRequestResultURL() {
		validateRequestResultURL = ok;
		
		if (getRequestResultURL() != null) {
			if (getRequestResultURL().equals("")) {
				validateRequestResultURL = idle;
				return validateRequestResultURL;
			}
		} else {
			validateRequestResultURL = idle;
			return validateRequestResultURL;
		}
		
		try {
			new URL(getRequestResultURL());
		} catch (MalformedURLException e) {
			validateRequestResultURL = badURL;
			e.printStackTrace();
		} finally {
			return validateRequestResultURL;
		}
	}
	/*
	@SuppressWarnings("finally")
	public String getValidateRequestArchiveURL() {
		validateRequestArchiveURL = ok;
		
		if (getRequestArchiveURL() != null) {
			if (getRequestArchiveURL().equals("")) {
				validateRequestArchiveURL = idle;
				return validateRequestArchiveURL;
			}
		} else {
			validateRequestArchiveURL = idle;
			return validateRequestArchiveURL;
		}
		
		try {
			new URL(getRequestArchiveURL());
		} catch (MalformedURLException e) {
			validateRequestArchiveURL = badURL;
			e.printStackTrace();
		} finally {
			return validateRequestArchiveURL;
		}
	}
	
	@SuppressWarnings("finally")
	public String getValidateCountArchiveAuctions() {
		int caa = 0;
		validateCountArchiveAuctions = "ok";
		
		if (getCountArchiveAuctions() != null) {
			if (getCountArchiveAuctions().equals("")) {
				validateCountArchiveAuctions = idle;
				return validateCountArchiveAuctions;
			}
		} else {
			validateCountArchiveAuctions = idle;
			return validateCountArchiveAuctions;
		}
		
		try {
			caa = Integer.parseInt(countArchiveAuctions);
			if (caa < 1) validateCountArchiveAuctions = badRange;
		} catch (NumberFormatException e) {
			validateCountArchiveAuctions = badNumber;
			e.printStackTrace();
		} finally {
			return validateCountArchiveAuctions;
		}
		
	}
	*/
	
	public static String getCurrentTypeAuctions() {
		return currentTypeAuctions;
	}
	public static String getArchiveTypeAuctions() {
		return archiveTypeAuctions;
	}
	public static String getBadRange() {
		return badRange;
	}
	public static String getBadNumber() {
		return badNumber;
	}
	public static String getBadURL() {
		return badURL;
	}
	public static String getIdle() {
		return idle;
	}
	public static String getOk() {
		return ok;
	}
	
	public boolean isValidAll() {
		if (!getValidateRequestFilingURL().equals(ok)) validAll = false;
		if (!getValidateRequestProcessingURL().equals(ok)) validAll = false;
		if (!getValidateRequestResultURL().equals(ok)) validAll = false;
		//if (!getValidateRequestArchiveURL().equals(ok)) validAll = false;
		//if (!getValidateCountArchiveAuctions().equals(ok)) validAll = false;
		return validAll;
	}
	
	public static String getContextPath() {
		return contextPath;
	}
	public static void setContextPath(String contextPath) {
		EditBean.contextPath = contextPath;
	}
}
