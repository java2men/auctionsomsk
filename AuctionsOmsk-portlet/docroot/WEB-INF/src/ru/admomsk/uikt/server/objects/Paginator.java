package ru.admomsk.uikt.server.objects;

public class Paginator {
	
	//private static String countRowsInPage = "10";
	private String total;
	private String current;
	private String offset;
	private String rows;
	private int step = 5;
	
	public int getIntTotal() {
		int intTotal = -1;
		if (total != null) intTotal = Integer.valueOf(total); 
		return intTotal;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getCurrent() {
		return current;
	}
	public void setCurrent(String current) {
		this.current = current;
	}
	public String getOffset() {
		return offset;
	}
	public void setOffset(String offset) {
		this.offset = offset;
	}
	public String getRows() {
		return rows;
	}
	public void setRows(String rows) {
		this.rows = rows;
	}
	public String getPrevious() {
		return String.valueOf(Integer.valueOf(current) - 1);
	}
	public String getNext() {
		return String.valueOf(Integer.valueOf(current) + 1);
	}
	public String getStart() {
		return "1";
	}
	public String getEnd() {
		return total;
	}
	public String[] getNavigationPage() {
		
		//расчитать m, округлив до целого в правую сторону, чтобы найти границы
		int m = (int)Math.ceil((double)Integer.valueOf(current)/(double)step);
		//найти границы номеров страниц, в зависимости от номера текущей страницы и шага h
		int b = m*step;
		int a = b-(step-1);
		
		int h;
		if (b > Integer.valueOf(total)) h = Integer.valueOf(total) - a + 1;
		else h = step;
		
		String[] navigationPage = new String[h];
		for (int i = 0; i < h; i++) {
			navigationPage[i] = String.valueOf(a+i);
		}
		
		return navigationPage;
	}
	
	
}
